from pitchblack.error import *
from pitchblack.lexer import Token
from pitchblack.parser import Parser, State
import unittest


def token_list(*tokens):
    return [t if type(t) is tuple else (t, '') for t in tokens]


class TestParser(unittest.TestCase):
    def setUp(self):
        self.parser = Parser()

    def test_documentation(self):
        tokens = token_list(Token.LITERAL,
                Token.BEGIN_DOC,
                Token.BEGIN_DEF,
                Token.BEGIN_DOC,
                Token.END_DEF,
                Token.END_DOC,
                Token.PRIMITIVE)
        expects = [Token.LITERAL,
                Token.BEGIN_DOC,
                Token.WORD,
                Token.WORD,
                Token.WORD,
                Token.END_DOC,
                Token.PRIMITIVE]
        tokens = [x[0] for x in self.parser.parse(tokens)]
        for result, expected in zip(tokens, expects):
            self.assertEqual(result, expected)

    def test_invalid_doc_close_error(self):
        with self.assertRaises(InvalidDocCloseError):
            self.parser.parse([(Token.END_DOC, ')')])

    def test_definition(self):
        tokens = token_list(Token.BEGIN_DEF,
                (Token.WORD, 'NAME'),
                Token.BEGIN_DOC,
                Token.END_DOC,
                Token.END_DEF)
        expects = [(Token.BEGIN_DEF, ''),
                (Token.DEF_NAME, 'NAME'),
                (Token.BEGIN_DOC, ''),
                (Token.END_DOC, ''),
                (Token.END_DEF, '')]
        tokens = self.parser.parse(tokens)
        for result, expected in zip(tokens, expects):
            self.assertEqual(result, expected)

    def test_redefine_primitive(self):
        tokens = token_list(Token.BEGIN_DEF,
                (Token.PRIMITIVE, 'SWAP'),
                Token.BEGIN_DOC,
                Token.LITERAL,
                Token.END_DOC,
                Token.END_DEF)
        expects = [(Token.BEGIN_DEF, ''),
                (Token.DEF_NAME, 'SWAP'),
                (Token.BEGIN_DOC, ''),
                (Token.WORD, ''),
                (Token.END_DOC, ''),
                (Token.END_DEF, '')]
        tokens = self.parser.parse(tokens)
        for result, expected in zip(tokens, expects):
            self.assertEqual(result, expected)

    def test_invalid_def_close_error(self):
        with self.assertRaises(InvalidDefCloseError):
            self.parser.parse([(Token.BEGIN_DEF, ':'),
                (Token.WORD, 'NAME'),
                (Token.BEGIN_BLOCK, 'DO'),
                (Token.END_DEF, ';')])

    def test_invalid_definition_error(self):
        with self.assertRaises(InvalidDefinitionError):
            self.parser.parse([(Token.BEGIN_DEF, ':'),
                    (Token.WORD, 'NAME'), (Token.BEGIN_DEF, ':')])

    def test_invalid_block_close_error(self):
        with self.assertRaises(InvalidBlockCloseError):
            self.parser.parse([(Token.END_BLOCK, 'END')])
