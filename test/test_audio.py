from pitchblack.audio import TonePlayer
import sys
import unittest


class TestAudio(unittest.TestCase):
    @unittest.skipUnless('test_audio_manually' in sys.argv, 'run with -k test_audio_manually')
    def test_audio_manually(self):
        # just a quick manual test to make sure the audio output is working
        s = TonePlayer(volume=0.1)
        for freq in [50, 0, 100, 0, 200, 0, 400, 0, 800, 0, 1200]:
            s.play(freq, 0.3)
