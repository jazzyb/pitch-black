from pitchblack.code import MorseEncoder
from pitchblack.error import InvalidCharacterError
import unittest


class TestMorseEncoder(unittest.TestCase):
    def setUp(self):
        self.encoder = MorseEncoder(1)

    def test_word_space(self):
        self.assertEqual(self.encoder.word_space(), (0, 7))

    def test_encode(self):
        expected = [(1000, 1),(0, 1),(1000, 1),(0, 1),(1000, 3),(0, 1),(1000, 1),(0, 3),(1000, 3),(0, 1),(1000, 3),(0, 1),(1000, 3),(0, 3),(1000, 3),(0, 1),(1000, 3),(0, 1),(1000, 3),(0, 7),(1000, 3),(0, 1),(1000, 1),(0, 1),(1000, 1),(0, 1),(1000, 1),(0, 3),(1000, 1),(0, 1),(1000, 3),(0, 3),(1000, 1),(0, 1),(1000, 3),(0, 1),(1000, 1)]
        self.assertEqual(self.encoder.encode('FOO BAR', 1000), expected)
