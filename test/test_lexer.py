from pitchblack.lexer import Lexer, InvalidCharacterError, Token
import unittest


class TestLexer(unittest.TestCase):
    def setUp(self):
        self.lexer = Lexer()

    def test_scan(self):
        s = "    :\tover\t( a b -- a b a )\nswap dup rot rot ;\n\ndo 1 2 over end\n"
        tokens = self.lexer.scan(s)
        self.assertEqual(tokens[0], (Token.BEGIN_DEF, ':'))
        self.assertEqual(tokens[1], (Token.WORD, 'OVER'))
        self.assertEqual(tokens[2], (Token.BEGIN_DOC, '('))
        self.assertEqual(tokens[3], (Token.WORD, 'A'))
        self.assertEqual(tokens[4], (Token.WORD, 'B'))
        self.assertEqual(tokens[5], (Token.WORD, '--'))
        self.assertEqual(tokens[6], (Token.WORD, 'A'))
        self.assertEqual(tokens[7], (Token.WORD, 'B'))
        self.assertEqual(tokens[8], (Token.WORD, 'A'))
        self.assertEqual(tokens[9], (Token.END_DOC, ')'))
        self.assertEqual(tokens[10], (Token.PRIMITIVE, 'SWAP'))
        self.assertEqual(tokens[11], (Token.PRIMITIVE, 'DUP'))
        self.assertEqual(tokens[12], (Token.PRIMITIVE, 'ROT'))
        self.assertEqual(tokens[13], (Token.PRIMITIVE, 'ROT'))
        self.assertEqual(tokens[14], (Token.END_DEF, ';'))
        self.assertEqual(tokens[15], (Token.BEGIN_BLOCK, 'DO'))
        self.assertEqual(tokens[16], (Token.LITERAL, '1'))
        self.assertEqual(tokens[17], (Token.LITERAL, '2'))
        self.assertEqual(tokens[18], (Token.WORD, 'OVER'))
        self.assertEqual(tokens[19], (Token.END_BLOCK, 'END'))

    def test_invalid_morse_error(self):
        with self.assertRaises(InvalidCharacterError):
            self.lexer.scan(": * swap dup + swap 1 - dup 1 = 'drop 'loop if ;")
