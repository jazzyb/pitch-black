from pitchblack.error import SegfaultError
from pitchblack.heap import Heap
import unittest


class TestHeap(unittest.TestCase):
    def setUp(self):
        self.heap = Heap(start=0x3000, cell_size=8, num_cells=4, endianness='big')

    def test_sbrk(self):
        heap = Heap()
        self.assertEqual(heap.sbrk(16), 0)
        self.assertEqual(heap.sbrk(0), 16)
        self.assertEqual(heap.sbrk(), 16)

    def test_store_fetch(self):
        self.assertEqual(self.heap.fetch(0x3008), 0)
        self.heap.store(0x3008, 0x0afebabedeadbeaf)
        self.assertEqual(self.heap.fetch(0x3008), 0x0afebabedeadbeaf)
        self.assertEqual(self.heap.fetch(0x300c, signed=False), 0xdeadbeaf00000000)

    def test_store_fetch_byte(self):
        self.assertEqual(self.heap.fetch_byte(0x3008), 0)
        self.heap.store_byte(0x3008, 0xff)
        self.assertEqual(self.heap.fetch_byte(0x3008), 0xff)

    def test_cell_size_error(self):
        with self.assertRaises(RuntimeError):
            Heap(cell_size=16)

    def test_endianness_error(self):
        with self.assertRaises(RuntimeError):
            Heap(endianness='default')

    def test_segfault_error(self):
        with self.assertRaises(SegfaultError):
            self.heap.fetch(0x4000)
        with self.assertRaises(SegfaultError):
            self.heap.fetch(0x301c)
        with self.assertRaises(SegfaultError):
            self.heap.fetch_byte(0x3020)
