# Elevator Pitch(Black)

PitchBlack is a stack-based programming language -- borrowing heavily from
Forth and Factor -- which is meant to be *listened to* via Morse Code rather
than read.  It is my contribution to the ["Slow Computing" aspect of The
Screenless Office](http://screenl.es/slow.html).  By approaching programming
from a completely different direction, I hope to discover a more deliberate,
mindful way of interacting with the computer.  "A language that doesn't affect
the way you think about programming, is not worth knowing." -- Alan Perlis

# Brainstorming

What I would like to see in it (and its IDE):
* Stack-based -- I chose this paradigm so as not to burden the programmer with
  keeping track of variables.  A good Forth program shouldn't keep more than 4
  or 5 values on the stack at any one time, and Forth encourages brevity of
  subroutines and discourages horizontal formatting which would not translate
  aurally.
* Recursion (with tail call optimization) instead of loops -- Anything that
  would encourage indentation in a "normal" language is discouraged.
  I suspect recursion -- especially if it can be written as a tail call --
  would be easier to understand aurally.
* Brevity of subroutine names -- In a visual language, a function like
  `getCustomerOrders` is good because it is self-documenting.  However, in
  Morse Code it will take several seconds to "present" this name to the
  programmer -- even at an expert clip.  PitchBlack should encourage short
  subroutine names.  There are a few ways we might be able to accomplish that:
  * Shorten the *presentation* of the subroutine name -- play only the first
    three or so characters of a name or as many characters as makes it unique
  * Namespaces -- the same name can reference different subroutines in
    different contexts
  * Types -- types could allow overloading of subroutine names as long as
    documenting the type information does not become an additional burden to
    the programmer
    * The more I think about it, the more I think strong typing is the best
      approach.  Let's say I have a subroutine that creates a list and puts it
      onto the stack, another routine that puts a string onto the stack, and
      finally a routine that inserts the string as the only item in the list.
      As I'm developing this code, I want to be able to check the state of the
      stack.  If I have no typing, all I will see is pointers on the stack
      without any context about which is which datastructure.  I won't be able
      to tell "at a glance" if I have my arguments on the stack swapped or
      not.  But if they are typed, the runtime can at least let me know that
      one is a list and the other is a string.
  * Macros/DSLs???
* Syntax highlighting -- When presenting information about the source code to
  the user, use different tones/pitches/etc. for subroutine names, literals,
  primitive keywords, etc.
* Visual debugging mode for the editor -- There are times when I won't catch
  something -- especially early when I don't know Morse Code or the language.
  I'll need a good visual display to fall back on, that can then be dismissed
  once I've gotten my bearings.
* Integrated documentation -- "If the language comes with its own way of
  documenting functions/classes/modules/whatever and it comes even with the
  simplest doc generator, you can be sure that all the language
  functions/classes/modules/libraries/frameworks will have a good
  documentation (not great, but at least good)."
  -- [Julio Biason](https://blog.juliobiason.net/thoughts/things-i-learnt-the-hard-way/)
  * Documentation instead of comments -- comments interspersed throughout a
    subroutine will hinder rather than help in an aural language while you're
    trying to listen to what a subroutine does
  * Since we can't see the code or listen to it quickly, we need good aural
    documentation.
  * Allow verbal audio recordings as subroutine/module level documentation.
* Do not store source code in text files -- There is no horizontal formatting
  in PitchBlack.  We can't look up and down the page on the display for
  context.  Nor can we glance at a directory listing for a file.  Maybe keep
  all the source code/tests/documentation in a single sqlite file and provide
  good methods in the IDE for moving around the source (think ctags).
  * Will need to write our own version control in this case.
* Integrated tests -- "You can be sure that if a language brings a testing
  framework -- even minimal -- in its standard library, the ecosystem around
  it will have better tests than a language that doesn't carry a testing
  framework, no matter how good the external testing frameworks for the
  language are."
  -- [Julio Biason](https://blog.juliobiason.net/thoughts/things-i-learnt-the-hard-way/)
* Time-traveling debugger -- Since listening to state can be slow, we may need
  a way of quickly replaying parts of the execution over and over again to
  check different effects on the running process (stack, rstack, heap, etc.)
  and to change state manually and replay if need be.
  * Builtin monitoring of an already running process
  * Q:  Does one even need logging if the time-traveling debugger is just
    always running?  (Yes, I know the debugger will of course need to forget
    things as its memory resources grow.)
* Convert debugging sessions/state to tests -- The less boiler-plate to keep
  track of, the better.  To this end, we should provide a way for the
  programmer to automatically convert the program state or a debugging session
  into a new test.
* Git for Ham -- It would be fun (*not necessary*) to present a program as a
  distributed repository and to push/pull/merge with other repos over CW
  traffic networks.
