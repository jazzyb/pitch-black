from pitchblack.error import SegfaultError
import struct


class Heap(object):
    def __init__(self, start=0, cell_size=8, num_cells=0, endianness='native'):
        assert start >= 0
        self._set_cell_size(cell_size, endianness)
        self._start = start
        self._mem = bytearray(num_cells * cell_size)

    def sbrk(self, nbytes=0):
        """Increase the size of the heap by nbytes"""
        assert nbytes >= 0
        prev_end = self._start + len(self._mem)
        if nbytes:
            self._mem += bytearray(nbytes)
        return prev_end

    def fetch(self, addr, signed=True):
        """Fetch a cell-sized value from addr"""
        self._check_address(addr)
        return self._unpack_cell(self._offset(addr), signed)

    def store(self, addr, value, signed=True):
        """Store value into the cell at addr"""
        self._check_address(addr)
        self._pack_cell(self._offset(addr), value, signed)

    def fetch_byte(self, addr):
        """Fetch a byte from addr"""
        self._check_address(addr)
        return self._mem[addr - self._start]

    def store_byte(self, addr, byte):
        """Store byte into addr"""
        self._check_address(addr)
        self._mem[addr - self._start] = byte

    def _set_cell_size(self, cell_size, endianness):
        """Set and validate the cell size"""
        self._cells = cell_size
        try:
            self._fmt = {'native': '@', 'big': '>', 'little': '<'}[endianness]
        except KeyError:
            raise RuntimeError("endianness must be one of 'native', 'big', or 'little'")
        try:
            self._fmt += {2: 'h', 4: 'i', 8: 'q'}[self._cells]
        except KeyError:
            raise RuntimeError('cell size must be one of 2, 4, or 8')

    def _check_address(self, addr):
        """Make sure address is within the bounds of our memory"""
        if addr < self._start or addr >= self._start + len(self._mem):
            raise SegfaultError('0x%x points to unallocated memory' % addr)

    def _offset(self, addr):
        """Return offset into memory array of cell at addr"""
        idx = addr - self._start
        if idx + self._cells > len(self._mem):
            raise SegfaultError('cell bounds of 0x%x extend beyond allocated memory' % addr)
        return idx

    def _unpack_cell(self, offset, signed):
        fmt = self._fmt if signed else self._fmt.upper()
        return struct.unpack_from(fmt, self._mem, offset)[0]

    def _pack_cell(self, offset, value, signed):
        fmt = self._fmt if signed else self._fmt.upper()
        struct.pack_into(fmt, self._mem, offset, value)
