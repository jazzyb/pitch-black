# LEXING
class InvalidCharacterError(RuntimeError):
    """Character does not have a Morse encoding"""
    pass

# PARSING
class InvalidDocCloseError(RuntimeError):
    """Parsed ')' before a corresponding '('"""
    pass

class InvalidDefCloseError(RuntimeError):
    """Parsed ';' before a corresponding ':'"""
    pass

class InvalidDefinitionError(RuntimeError):
    """':' definition parsed outside of top-level domain"""
    pass

class InvalidBlockCloseError(RuntimeError):
    """Parsed END before a corresponding DO"""
    pass

# MEMORY
class SegfaultError(RuntimeError):
    """Accessed unallocated memory"""
    pass
