from pitchblack.code import CODES
from pitchblack.error import InvalidCharacterError
from enum import Enum, auto
import re


PRIMITIVES = set([
        # KEYWORDS
        ':', ';', 'DO', 'END', '(', ')',

        # INTEGER MATH
        '+',        # add
        '-',        # subtract
        'X',        # multiply
        '/',        # divide
        '0/0',      # modulo
        # : T/T ( modulo with cut numbers ) 0/0 ;

        # INTEGER COMPARISON
        '=', 'LT', 'GT',

        # BOOLEAN LOGIC
        'NOT',

        # STACK PRIMITIVES
        'DROP',     # ( a -- )
        'DUP',      # ( a -- a a )
        'ROT',      # ( a b c -- b c a )
        'SWAP',     # ( a b -- b a )
        # : -ROT ( a b c -- c a b ) ROT ROT ;
        # : TUCK ( a b -- b a b ) DUP -ROT ;
        # : OVER ( a b -- a b a ) SWAP TUCK ;
        # etc...

        # BLOCK PRIMITIVES
        'CALL',     # ( [... f] -- result )
        'CURRY',    # ( a [... f] -- [a ... f] )
        'LEN',      # ( [a_1 a_2 ... a_n] -- n )
        'STEP',     # ( [a b ... f] -- a [b ... f] )
        'UNLESS',   # ( flag [... f] -- ) execute block if flag is zero

        # MEMORY PRIMITIVES
        '!',        # ( value address -- ) stores cell value into address
        '@',        # ( address -- value ) loads cell value from address onto the stack
        'C!',       # ( byte address -- ) stores single byte into address
        'C@',       # ( address -- byte ) loads byte from address onto the stack
        'CELLS',    # ( n -- 8*n ) return size, in bytes, of a cell
        'SBRK',     # ( nbytes -- address ) increments the program break by
                    #                       nbytes and places the *previous*
                    #                       program break on the stack
        # forth primitives HERE and ALLOT can be written in terms of SBRK:
        # : HERE ( -- address ) 0 SBRK ;
        # : ALLOT ( nbytes -- ) SBRK DROP ;
])


class Token(Enum):
    BEGIN_DEF = auto()
    END_DEF = auto()
    DEF_NAME = auto()
    BEGIN_BLOCK = auto()
    END_BLOCK = auto()
    BEGIN_DOC = auto()
    END_DOC = auto()
    LITERAL = auto()
    PRIMITIVE = auto()
    WORD = auto()


class Lexer(object):
    def __init__(self):
        self._valid_characters = set(CODES)
        self._valid_characters.add(' ')

    def scan(self, string):
        """Convert string to a list of (TOKEN, "WORD") tuples"""
        tokens = []
        current = ''
        for col, ch in enumerate(self._sanitize(string)):
            self._validate(ch, col)
            if ch == ' ':
                tokens.append(self._tokenize(current))
                current = ''
            else:
                current += ch
        if current:
            tokens.append(self._tokenize(current))
        return tokens

    def _sanitize(self, string):
        """Convert string to "morse-code-friendly" characters"""
        return re.sub(r'\s+', ' ', string.strip().upper())

    def _validate(self, ch, col):
        """Make sure that ch is a known morse code character"""
        if ch not in self._valid_characters:
            # FIXME when we start relying on aural output, we don't want to
            # raise string errors
            raise InvalidCharacterError("'%c' at column %d is not a valid morse code" % (ch, col))

    def _tokenize(self, word):
        """Return a token tuple for word"""
        return (self._identify(word), word)

    def _identify(self, word):
        """Return the token value of word"""
        if word == ':':
            return Token.BEGIN_DEF
        if word == ';':
            return Token.END_DEF
        if word == 'DO':
            return Token.BEGIN_BLOCK
        if word == 'END':
            return Token.END_BLOCK
        if word == '(':
            return Token.BEGIN_DOC
        if word == ')':
            return Token.END_DOC

        # FIXME right now the only literals are integers
        try:
            int(word)
        except:
            return Token.PRIMITIVE if word in PRIMITIVES else Token.WORD
        else:
            return Token.LITERAL
