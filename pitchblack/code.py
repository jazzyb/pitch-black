from pitchblack.error import InvalidCharacterError


CODES = {
        # LETTERS
        'A': '.=',
        'B': '=...',
        'C': '=.=.',
        'D': '=..',
        'E': '.',
        'F': '..=.',
        'G': '==.',
        'H': '....',
        'I': '..',
        'J': '.===',
        'K': '=.=',
        'L': '.=..',
        'M': '==',
        'N': '=.',
        'O': '===',
        'P': '.==.',
        'Q': '==.=',
        'R': '.=.',
        'S': '...',
        'T': '=',
        'U': '..=',
        'V': '...=',
        'W': '.==',
        'X': '=..=',
        'Y': '=.==',
        'Z': '==..',

        # NUMBERS
        '1': '.====',
        '2': '..===',
        '3': '...==',
        '4': '....=',
        '5': '.....',
        '6': '=....',
        '7': '==...',
        '8': '===..',
        '9': '====.',
        '0': '=====',

        # PUNCTUATION
        '.': '.=.=.=',
        ',': '==..==',
        ':': '===...',
        '?': '..==..',
        "'": '.====.',
        '-': '=....=',
        '/': '=..=.',
        '(': '=.==.',
        ')': '=.==.=',
        '"': '.=..=.',
        '=': '=...=',
        '+': '.=.=.',
        '&': '.=...',
        '@': '.==.=.',

        # PUNCTUATION (unofficial -- not in ITU-R)
        '!': '=.=.==',
        ';': '=.=.=.',
        '_': '..==.=',
        '$': '...=..=',
}

CUTS = {
        # CUT NUMBERS
        '1': 'A',
        '2': 'U',
        '3': 'V',
        '4': '4',
        '5': 'E',
        '6': '6',
        '7': 'G',
        '8': 'D',
        '9': 'N',
        '0': 'T',
}


class MorseEncoder(object):
    """Provide morse encodings of strings"""

    def __init__(self, duration):
        """duration is a float representing seconds"""
        self.duration = duration

    def encode(self, string, frequency):
        """Return an array of (frequency, time) representing the morse
        encoding of string
        """
        code = []
        words = string.strip().split()
        for word in words[:-1]:
            code.extend(self._encode_word(word, frequency))
            code.append(self.word_space())
        return code + self._encode_word(words[-1], frequency)

    def word_space(self):
        """Length of silence between words"""
        return (0, self.duration * 7)

    def _encode_word(self, word, frequency):
        """Return morse encoding of word"""
        code = []
        for ch in word[:-1]:
            code.extend(self._encode_character(ch, frequency))
            code.append(self._character_space())
        return code + self._encode_character(word[-1], frequency)

    def _character_space(self):
        """Length of silence between characters"""
        return (0, self.duration * 3)

    def _encode_character(self, ch, frequency):
        """Return morse encoding of ch"""
        code = []
        for i in self._dots_and_dashes(ch):
            duration = self.duration if i == '.' else self.duration * 3
            code.append((frequency, duration))
            code.append((0, self.duration))
        # return code *minus* the last space between dots/dashes
        return code[:-1]

    def _dots_and_dashes(self, ch):
        """Return corresponding /[.=]+/ string if ch has a morse encoding"""
        try:
            return CODES[ch]
        except KeyError:
            raise InvalidCharacterError("'%c' does not have a morse encoding" % ch)
