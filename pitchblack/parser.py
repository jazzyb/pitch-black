from pitchblack.error import (
        InvalidBlockCloseError,
        InvalidDocCloseError,
        InvalidDefCloseError,
        InvalidDefinitionError
)
from pitchblack.lexer import Token
from enum import Enum, auto


class State(Enum):
    BLOCK = auto()
    DEF_DECLARATION = auto()
    DEFINITION = auto()
    DOCUMENTATION = auto()
    EMPTY = auto()


class Parser(object):
    def __init__(self):
        self.clear()

    def clear(self):
        """Reset the parser's state"""
        self._stack = [State.EMPTY]

    @property
    def state(self):
        """Current state of the parser"""
        return self._stack[-1]

    def parse(self, input_tokens):
        """Validate the token array"""
        count = 0
        parsed_tokens = []
        for item in input_tokens:
            count += 1
            parsed_tokens.append(item)
            token, word = item

            # NEW DEFINITION:  This comes first to allow us to redefine words.
            if self.state is State.DEF_DECLARATION:
                # mark the name of the new definition
                parsed_tokens[-1] = (Token.DEF_NAME, word)
                self._stack[-1] = State.DEFINITION

            # DOCUMENTATION
            elif token is Token.END_DOC:
                if self.state is State.DOCUMENTATION:
                    self._stack.pop()
                else:
                    # ) can only occur when state is DOCUMENTATION
                    raise InvalidDocCloseError('WORD %d' % count)
            elif self.state is State.DOCUMENTATION:
                # everything in DOCUMENTATION is *just* a WORD
                parsed_tokens[-1] = (Token.WORD, word)
            elif token is Token.BEGIN_DOC:
                self._stack.append(State.DOCUMENTATION)

            # DEFINITIONS
            elif token is Token.END_DEF:
                if self.state is State.DEFINITION:
                    self._stack.pop()
                else:
                    # ; can only occur when state is DEFINITION
                    raise InvalidDefCloseError('WORD %d' % count)
            elif token is Token.BEGIN_DEF:
                if self.state is State.EMPTY:
                    self._stack.append(State.DEF_DECLARATION)
                else:
                    # : can only occur when state is EMPTY
                    raise InvalidDefinitionError('WORD %d' % count)

            # BLOCKS
            elif token is Token.END_BLOCK:
                if self.state is State.BLOCK:
                    self._stack.pop()
                else:
                    # END can only occur when state is BLOCK
                    raise InvalidBlockCloseError('WORD %d' % count)
            elif token is Token.BEGIN_BLOCK:
                self._stack.append(State.BLOCK)

        return parsed_tokens
