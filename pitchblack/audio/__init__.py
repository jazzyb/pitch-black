try:
    from pitchblack.audio.pyaudiostream import AudioStream
except ImportError:
    raise # TODO use kivy/audiostream


try:
    # try to import numpy
    from pitchblack.audio.numpytone import SineTone
except ImportError:
    # fallback to slower stdlib version
    from pitchblack.audio.stdlibtone import SineTone


class TonePlayer(object):
    """Generate tones of a certain frequency and duration in seconds

    Subclasses must implement _sine_wave(), _fade_tone(), and _bytes()
    """

    def __init__(self, volume=0.25, rate=44100, fade=100, channels=1):
        assert type(volume) is float and (0.0 <= volume <= 1.0)
        assert fade > 0
        # see http://en.wikipedia.org/wiki/Bit_rate#Audio
        self._sin = SineTone(volume, rate, fade)
        self._stream = AudioStream(channels, rate)

    @property
    def volume(self):
        return self._sin.volume

    @volume.setter
    def volume(self, volume):
        self._sin.volume = volume

    def play(self, frequency, duration):
        """Play a single sine wave tone"""
        # see http://www.phy.mtu.edu/~suits/notefreqs.html
        points = self._sin.sine_wave(frequency, duration)
        points = self._sin.fade_tone(points)    # fade ends to reduce clicking
        data = self._sin.to_bytes(points)          # convert to bytestream of float32s
        self._stream.write(data)
