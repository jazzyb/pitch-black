import decimal
import math
import struct


class SineTone(object):
    """Generate sine wave with python stdlib"""

    def __init__(self, volume, rate, fade):
        self.volume = volume
        self._rate = rate
        self._fade = fade

    def sine_wave(self, frequency, duration):
        length = int(duration * self._rate)
        factor = float(frequency) * (2 * math.pi) / self._rate
        return [self.volume * math.sin(float(i) * factor) for i in range(length)]

    def fade_tone(self, points):
        fade_in = zip(points[:self._fade], self._frange(0.0, 1.0, 1/float(self._fade)))
        fade_out = zip(points[-self._fade:], self._frange(1.0, 0.0, -1/float(self._fade)))
        points[:self._fade] = [p * f for p, f in fade_in]
        points[-self._fade:] = [p * f for p, f in fade_out]
        return points

    def to_bytes(self, points):
        return struct.pack('%df' % len(points), *points)

    def _frange(self, start, end, jump):
        """range() but for floating point numbers"""
        i, end, jump = map(lambda f: decimal.Decimal('%.3f' % f), (start, end, jump))
        while (jump < 0 and i > end) or (jump > 0 and i < end):
            yield float(i)
            i += jump
