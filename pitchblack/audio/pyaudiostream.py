import pyaudio

class AudioStream(object):
    def __init__(self, channels, rate):
        self._channels = channels
        self._rate = rate
        self._p = pyaudio.PyAudio()
        self._stream = self._p.open(format=pyaudio.paFloat32,
                channels=self._channels,
                rate=self._rate,
                output=True)

    def __del__(self):
        self._stream.stop_stream()
        self._stream.close()
        self._p.terminate()

    def write(self, data):
        self._stream.write(data)
