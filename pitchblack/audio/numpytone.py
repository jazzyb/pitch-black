import numpy


class SineTone(object):
    """Generate sine wave quickly with numpy"""

    def __init__(self, volume, rate, fade):
        self.volume = volume
        self._rate = rate
        self._fade = fade

    def sine_wave(self, frequency, duration):
        length = int(duration * self._rate)
        factor = float(frequency) * (2 * numpy.pi) / self._rate
        points = [numpy.sin(numpy.arange(length) * factor)]
        return numpy.concatenate(points) * self.volume

    def fade_tone(self, points):
        fade_in = numpy.linspace(0.0, 1.0, num=self._fade, endpoint=False)
        fade_out = numpy.linspace(1.0, 0.0, num=self._fade, endpoint=False)
        points[:self._fade] = numpy.multiply(points[:self._fade], fade_in)
        points[-self._fade:] = numpy.multiply(points[-self._fade:], fade_out)
        return points

    def to_bytes(self, points):
        return points.astype(numpy.float32).tostring()
